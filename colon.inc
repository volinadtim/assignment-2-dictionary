%define dict_key_ptr_offset 8

; Макрос `colon` принимает два аргумента: ключ и метку, которая будет сопоставлена значению.
; Эта метка не может быть сгенерирована из самого значения, так 
; как в строчке могут быть символы, которые не могут встречаться в метках, 
; например, арифметические знаки, знаки пунктуации и т.д. 
; После использования такого макроса можно напрямую указать значение, сопоставляемое ключу.
%define dict_ptr 0
%macro colon 2
    %ifstr %1
        %ifid %2
            %2: dq dict_ptr
                db %1, 0
            %define dict_ptr %2         ; Переопределяем указатель на "вершину" словаря
        %else
            %error "Second argument should be a label"
        %endif
    %else
        %error "First argument should be a string"
    %endif
%endmacro
