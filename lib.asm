#define space_symbol 0x20
#define new_line_symbol 0xA
#define tabulation_symbol 0x9
#define stdout_descriptor 1
#define stderr_descriptor 2
#define sys_write 1

; По xor-ам vs mov 0 написано вот тут
; https://stackoverflow.com/questions/1135679/does-using-xor-reg-reg-give-advantage-over-mov-reg-0
; Теперь я почти везде поменял на ксоры.

section .text
global exit
global string_length
global print_string
global print_error_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_string_err


; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    ; Можно было реализовать через mov и jz, но такой подход требует лишнюю операцию (перемещения)
    cmp byte[rdi+rax], 0
    je .break
    inc rax
    jmp .loop
.break:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi            ; Сохраняем состояние rdi, т.к. после вызова функции там может быть что угодно
    call string_length
    pop rsi             ; Начало строки
    mov rdi, stdout_descriptor          ; Дескриптор на stdout
    mov rdx, rax        ; Длина строки
    mov rax, sys_write          ; Системная команда write
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_error_string:
    push rdi            ; Сохраняем состояние rdi, т.к. после вызова функции там может быть что угодно
    call string_length
    pop rsi             ; Начало строки
    mov rdi, stderr_descriptor          ; Дескриптор на stderr
    mov rdx, rax        ; Длина строки
    mov rax, sys_write          ; Системная команда write
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
    push rdi            ; Т.к. мы должны получить ссылку на символ, положим его в стек
    mov rsi, rsp        ; Ссылка на символ
    pop rdi             ; Подчищаем стек
    mov rdi, 1          ; Дескриптор на stdout
    mov rdx, 1          ; Длина одного символа
    mov rax, 1          ; Системная команда write
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    ; Могли испольховать print_char, но так мы сэкономили одну команду mov
    push '\n'           ; Т.к. мы должны получить ссылку на символ, положим его в стек
    mov rsi, rsp        ; Ссылка на символ
    pop rdi             ; Подчищаем стек
    mov rdi, 1          ; Дескриптор на stdout
    mov rdx, 1          ; Длина одного символа
    mov rax, 1          ; Системная команда write
    syscall
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, rdi         ; Разбиваемое на цифры число
    mov r9, rsp         ; Сохраняем состояние стека
    mov r10, 10
    push 0x0
.loop:
    mov rax, r8         ; Делимое
    xor rdx, rdx        ; Очистка от мусора, т.к. div использует rax:rdx для деления
    div r10             ; В rax - результат, в rdx (dl) - остаток от деления
    mov r8, rax         ; Обновления делимое
    add dl, '0'         ; Код ASCII
    dec rsp             ; Выделяем место под следующую цифру
    mov [rsp], dl       ; Сохраняем цифру

    cmp r8, 0
    jne .loop           ; Если число ещё не закончилось, идём на след итерацию
.break:
    mov rdi, rsp

    push r9             ; Запоминаем r9 чтобы ничего плохо с ним не случилось
    call print_string
    pop r9
    
    mov rsp, r9         ; Восстанавливаем вершину стека
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov rax, rdi
    cmp rdi, 0
    jge .print_unsigned
.negative:
    neg rdi
    push rdi
    mov rdi, 45         ; ASCII код для '-'
    call print_char
    pop rdi
.print_unsigned:
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
.loop:
    ; Побайтовое сравнение
    mov dl, byte [rdi+rcx]
    cmp dl, byte [rsi+rcx]
    jne .fail

    ; Закончили ли
    cmp dl, 0x0
    je .success

    ; i++
    inc rcx
    jmp .loop
.success:
    mov rax, 1
    ret
.fail:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0x0            ; Место под чтение символа
    xor rax, rax        ; Системная команда read
    xor rdi, rdi        ; Дескриптор stdin
    mov rsi, rsp        ; Адрес для чтения на вершину стека
    mov rdx, 1          ; Строки длиной один байт
    syscall
    cmp rax, -1
    pop rax             ; Не изменяет флаги, поэтому сразу достаём значение со стека
    jne .return
    xor rax, rax        ; Обнуляем если файл кончился
.return:
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    dec rsi                 ; Оставляем место для нуль-терминатора
    ; Сохраняем регистры
    push rdi
    push rsi
.trim_loop:
    call read_char

    cmp rax, space_symbol
    je .trim_loop
    cmp rax, tabulation_symbol
    je .trim_loop
    cmp rax, new_line_symbol
    je .trim_loop

    ; Восстанавливаем регистры
    pop rsi
    pop rdi
    xor rcx, rcx
.loop:
    ; Откопали стоп-нуль
    cmp rax, 0
    je  .return
    ; Откопали пробел
    cmp rax, space_symbol
    je  .return
    cmp rax, tabulation_symbol
    je  .return
    cmp rax, new_line_symbol
    je  .return
    
    cmp rcx, rsi
    jg .fail
    mov [rdi+rcx], rax
    
    inc rcx
    
    ; Сохраняем регистры
    push rdi
    push rsi
    push rcx
    
    call read_char
    
    ; Восстанавливаем регистры
    pop rcx
    pop rsi
    pop rdi

    jmp .loop
.fail:
    xor rax, rax
    ret
.return:
    mov rdx, rcx
    mov byte[rdi+rcx+1], 0x0
    mov rax, rdi
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax                ; Буфер для числа
    xor rdx, rdx                ; Длина числа

.loop:
    mov r8b, [rdi]
    ; Проверяем, закончилось ли число
    cmp r8, '0'
    jl .return
    cmp r8, '9'
    jg .return

    sub r8, '0'

    ; Смещение буфера на один разряд (умножение на 10)
    shl rax, 1  ; *2    // 2
    lea rax, [rax+rax*4]

    ; Добавление текущего разряда
    add rax, r8

    ; Следующая итерация
    inc rdx
    inc rdi

    jmp .loop
.return:
    ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rdx, rdx
    mov r8b, [rdi]
    cmp r8, '+'
    je .positive
    cmp r8, '-'
    je .negative
.unsigned:
    call parse_uint
    ret
.positive:
    inc rdi
    call parse_uint
    inc rdx
    ret
.negative:
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    
    ; Вырыгиваем, если не помещается
    dec rdx                         ; Не забываем про нуль-терминатор
    cmp rax, rdx
    jg .fail

    xor rcx, rcx

.loop:
    mov r9b, byte[rdi + rcx]
    mov byte[rsi + rcx], r9b
    cmp rcx, rdx
    je .return

    inc rcx
    jmp .loop
.return:
    ret
.fail:
    xor rax, rax
    ret

; Принимает ссылку на буфер (rdi) и длину буфера (rsi).
; Считывает из стандартного потока ввода строку и записывает ее в буфер.
; Возвращает адрес буфера или 0, если строку не удалось прочитать (она длиннее буфера).
read_line:
    push r12
    push r13
    push rdi
    mov r12, rdi
    mov r13, rsi
    dec r13
.begin:
    call read_char
    test al, al
    jz .finish
    cmp al, new_line_symbol
    jz .finish
.write_char:
    test r13, r13
    jz .size_error
    mov byte[r12], al
    inc r12
    dec r13
    jmp .begin
.size_error:
    call read_char
    test al, al
    jz .handling
    cmp al, new_line_symbol
    jnz .size_error
.handling:
    pop rdi
    xor rax, rax
    jmp .end
.finish:
    pop rax
    mov byte[r12], 0
    jmp .end
.end:
    pop r13
    pop r12
    ret
