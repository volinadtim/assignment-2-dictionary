#!/usr/bin/python3

import subprocess
import unittest
import xmlrunner
from subprocess import CalledProcessError, Popen, PIPE
from enum import Enum


class Exceptions(Enum):
    InvalidInput = 'Invalid Input'
    InvalidKey = 'Invalid Key'


class IOLibraryTest(unittest.TestCase):
    def compile(self, main_fname, fname):
        self.assertEqual(
            subprocess.call(['nasm', '-f', 'elf64', fname + '.asm', '-o', fname + '.o']), 0, 'failed to compile'
        )
        # ld -o main main.o lib.o dict.o
        # self.assertEqual(subprocess.call(['ld', '-o', main_fname, fname, fname + '.o']), 0, 'failed to link')

    def launch(self, fname, input):
        output = b''
        try:
            p = Popen(['./' + fname], shell=None, stdin=PIPE, stdout=PIPE, stderr=PIPE)
            (output, error) = p.communicate(input.encode())
            self.assertNotEqual(p.returncode, -11, 'segmentation fault')
            error_decoded = '' if error is None else error.decode()
            return (output.decode(), error_decoded, p.returncode)
        except CalledProcessError as exc:
            self.assertNotEqual(exc.returncode, -11, 'segmentation fault')
            return (exc.output.decode(), '', exc.returncode)

    def perform(self, main_fname, fname, input):
        self.compile(main_fname, fname)
        self.assertEqual(subprocess.call(['ld', '-o', 'main', 'main.o', 'lib.o', 'dict.o']), 0, 'failed to link')
        return self.launch(fname, input)

    def test_main(self):
        inputs = [
            'and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on_and_on',
            'go_on',
            'and_on',
            '',
            'i_have_so_much_to_say_about_you_and_about_your_husband',
            'i_don\'t_exist_lol',
            'perfect',
            '私はアスキーのファンではありません',
        ]
        exp_out = [
            '',
            'Yeah, yeah, yeah',
            'we don\'t believe whats on TV.',
            'and what we want, we know we can\'t believe.',
            'we have all learned to kill our dreams.',
            '',
            '',
            '',
        ]
        exp_err = [
            Exceptions.InvalidInput.value,
            '',
            '',
            '',
            '',
            Exceptions.InvalidKey.value,
            Exceptions.InvalidKey.value,
            Exceptions.InvalidKey.value,
        ]
        entries = zip(inputs, exp_out, exp_err)
        for entry in list(entries):
            self.compile('main', 'lib')
            self.compile('main', 'dict')
            (output, error, code) = self.perform('main', 'main', entry[0])
            self.assertEqual(
                output.strip(), entry[1].strip(), 'program generated wrong output: %s' % repr(output.strip())
            )
            self.assertEqual(error.strip(), entry[2].strip(), 'program generated wrong error: %s' % repr(error.strip()))


if __name__ == '__main__':
    with open('report.xml', 'w') as report:
        unittest.main(testRunner=xmlrunner.XMLTestRunner(output=report), failfast=False, buffer=False, catchbreak=False)
