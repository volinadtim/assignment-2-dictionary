%include "dict.inc"
%include "lib.inc"
%include "words.inc"
%define BUFFER_CAPACITY 256

section .rodata
    invalid_input_error db "Invalid Input", 10, 0
    invalid_key db "Invalid Key", 10, 0

section .bss
    buffer resb BUFFER_CAPACITY

section .text
global _start

_start:
    mov rdi, buffer
    mov rsi, BUFFER_CAPACITY
    call read_line                  ; 0 -> rax, если не найден нуль-терминант
    test rax, rax
    jz .invalid_input
    mov rdi, buffer
    mov rsi, dict_ptr
    call find_word
    test rax, rax
    jz .not_found
    mov rdi, rax
    add rdi, dict_key_ptr_offset
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    call print_string
    jmp exit
.not_found:
    mov rdi, invalid_key
    call print_error_string
    jmp exit
.invalid_input:
    mov rdi, invalid_input_error
    call print_error_string
    jmp exit
