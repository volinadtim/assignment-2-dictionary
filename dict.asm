section .text
global find_word
%include "lib.inc"

; @param rdi - указатель на нуль-терминированную строку
; @param rsi - указатель на словарь [colon.inc]
; @return - указатель на начало вхождения в словарь. Если не найдено, то 0.
find_word:
    push r12
    push r13
    mov r12, rdi
    mov r13, rsi
.loop:
    test rsi, rsi
    jz .not_found
    add rsi, dict_key_ptr_offset              ; Пропускаем первые 4 слова - указатель на предыдущую запись в словаре
    call string_equals      ; Если равны, то 1 -> rax, иначе то 0 -> rax
    test rax, rax
    jnz .success
    mov rsi, [r13]          ; Откатываемся к более старому ключу
    mov r13, rsi
    jmp .loop
.not_found:
    xor rax, rax
    jmp .return
.success:
    mov rax, r13             ; Возвращаем ключ
.return:
    pop r13
    pop r12
    ret
