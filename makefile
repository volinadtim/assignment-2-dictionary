%.o: %.asm
	nasm -f elf64 -o $@ $<

programm: main.o lib.o dict.o
	ld -o main main.o lib.o dict.o

.PHONY: clean test

clean:
	rm -f *.o

test:
	make clean
	make programm
	python3 test.py
